public class BinaryTree {
    public Node root;

    // R S D
    public void preOrder(Node n) {
        if (n != null){
            System.out.print(n.value + " ");
            preOrder(n.left);
            preOrder(n.right);
        }
    }
    // S R D
    public void inOrder(Node n) {
        if (n != null){
            inOrder(n.left);
            System.out.print(n.value + " ");
            inOrder(n.right);
        }
    }

    // S D R
    public void postOrder(Node n){
        if (n != null){
            postOrder(n.left);
            postOrder(n.right);
            System.out.print(n.value + " ");
        }
    }

    public void init(){
        Node n8 = new Node(8);
        Node n2 = new Node(2);
        Node n13 = new Node(13);
        Node n22 = new Node(22);
        Node n3 = new Node(3);
        Node n20 = new Node(20);
        root = new Node(7);

        root.left = n2;
        root.right = n8;
        n2.left = n13;
        n2.right = n22;
        n22.left = n3;
        n8.left = n20;
    }
}