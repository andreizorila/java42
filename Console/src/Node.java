public class Node {
    public int value;
    public Node left;
    public Node right;

    public Node(int v) {
        this(v, null, null);

    }

    public Node(int v, Node lf, Node rt){
        this.value = v;
        this.left = lf;
        this.right = rt;
    }

}