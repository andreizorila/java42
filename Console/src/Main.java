public class Main {

    public static void main(String[] args) {
        BinaryTree b = new BinaryTree();
        b.init();

        System.out.println();
        b.preOrder(b.root);

        System.out.println();
        b.inOrder(b.root);

        System.out.println();
        b.postOrder(b.root);
    }
}
